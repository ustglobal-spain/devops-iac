# variable "client_secret" {
#   description = "Client Secret for SPN"
# }
# variable "client_id" {
#     description = "Client ID for SPN"
# }

# variable "subscription_id" {
#     description = "Subscription ID"
# }

# variable "tenant_id" {
#     description = "Tenant ID"
# }

variable "resource_group_name" {
    description = "resource group name"
}

variable "location" {
    description = "resource group name"
    default = "westeurope"
}

variable "web-linux-admin-username" {
    description = "resource group name"
    default = "admin"
}

variable "web-linux-vm-password" {
    description = "password "
    default = "P@ssword123"
}