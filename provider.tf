# provider "azurerm" {
# subscription_id = var.subscription_id
# client_id= var.client_id
# client_secret = var.client_secret
# tenant_id= var.tenant_id
# version="=1.44.0"
# }

provider "azurerm" {
  features {}
}

terraform {
required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=2.46.0"
    }
  }
}


# terraform{
# backend "azurerm" {
#         resource_group_name = ""
#         storage_account_name = ""
#         container_name = "tfstates"
#         key = "platform-dev.tfstate"
#         access_key = ""
# }
# }