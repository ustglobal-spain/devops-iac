data "azurerm_resource_group" "example" {
  name     = var.resource_group_name
}

# Create Security Group to access web
resource "azurerm_network_security_group" "web-linux-vm-nsg" {
  name = "web-linux-vm-nsg"
  location            = data.azurerm_resource_group.example.location
  resource_group_name = data.azurerm_resource_group.example.name
  security_rule {
    name                       = "allow-ssh"
    description                = "allow-ssh"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "Internet"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "allow-http"
    description                = "allow-http"
    priority                   = 110
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "80"
    source_address_prefix      = "Internet"
    destination_address_prefix = "*"
  }
}



# Create a subnet for Network
resource "azurerm_subnet" "network-subnet" {
  name                 = "subnet-linuxbox"
  address_prefixes       = ["172.19.1.0/24"]
  virtual_network_name = azurerm_virtual_network.network-vnet.name
  resource_group_name = data.azurerm_resource_group.example.name
}

# Create the network VNET
resource "azurerm_virtual_network" "network-vnet" {
  name                = "vnet_linux"
  address_space       = ["172.19.0.0/16"]
  location            = data.azurerm_resource_group.example.location
  resource_group_name = data.azurerm_resource_group.example.name
}


# Associate the web NSG with the subnet
resource "azurerm_subnet_network_security_group_association" "web-linux-vm-nsg-association" {
  depends_on=[azurerm_network_security_group.web-linux-vm-nsg]
  subnet_id                 = azurerm_subnet.network-subnet.id
  network_security_group_id = azurerm_network_security_group.web-linux-vm-nsg.id
}
# Get a Static Public IP
resource "azurerm_public_ip" "web-linux-vm-ip" {
  name = "linux-vm-ip"
  location            = data.azurerm_resource_group.example.location
  resource_group_name = data.azurerm_resource_group.example.name
  allocation_method   = "Static"
}

# Create Network Card for web VM
resource "azurerm_network_interface" "web-linux-vm-nic" {
  depends_on=[azurerm_public_ip.web-linux-vm-ip]
  name = "linux-vm-nic"
  location            = data.azurerm_resource_group.example.location
  resource_group_name = data.azurerm_resource_group.example.name
  
  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.network-subnet.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.web-linux-vm-ip.id
  }  
}
# # Data template Bash bootstrapping file
# data "template_file" "linux-vm-cloud-init" {
#   template = file("azure-user-data.sh")
# }

# Create Linux VM with web server
resource "azurerm_linux_virtual_machine" "web-linux-vm" {
  depends_on=[azurerm_network_interface.web-linux-vm-nic]
  name = "linux-vm-prudential"
  location            = data.azurerm_resource_group.example.location
  resource_group_name = data.azurerm_resource_group.example.name
  network_interface_ids = [azurerm_network_interface.web-linux-vm-nic.id]
  size                  = "Standard_B2s" #"Standard_F2"
  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  computer_name = "linux-vm"
  admin_username = var.web-linux-admin-username
  admin_password = var.web-linux-vm-password
#   custom_data = base64encode(data.template_file.linux-vm-cloud-init.rendered)
  disable_password_authentication = false
  }